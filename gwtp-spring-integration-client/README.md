# gwtp-spring-integration-client
Basic functionality to integrate gwtp with spring, client part.

It includes

* authentication integration with spring security
* user session handling
* validation handling and report to client
* rest callback implementations handling errors from server
* expose some interfaces to client

Maven integration
-----------------

The dependency itself for GWT-Projects:

```
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwtp-spring-integration-client</artifactId>
      <version>1.0.0</version>
    </dependency>
```

GWT Integration
---------------

You have to inherit GwtpSpringIntegration into your project .gwt.xml file:

```
<inherits name="de.knightsoftnet.gwtp.spring.GwtpSpringIntegration" />
```
