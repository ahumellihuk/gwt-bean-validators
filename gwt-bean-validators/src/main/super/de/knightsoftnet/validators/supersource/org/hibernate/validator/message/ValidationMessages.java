package org.hibernate.validator.message;

import com.google.gwt.i18n.client.ConstantsWithLookup;

/**
 * Interface to represent the constants contained in resource bundle:
 * 'validation/ValidationMessages.properties'.
 */
@SuppressWarnings({"checkstyle:abbreviationaswordinname", "PMD.MethodNamingConventions"})
public interface ValidationMessages extends ConstantsWithLookup {

  /**
   * Translated "must be false".
   *
   * @return translated "must be false"
   */
  @DefaultStringValue("must be false")
  @Key("javax.validation.constraints.AssertFalse.message")
  String javax_validation_constraints_AssertFalse_message();

  /**
   * Translated "must be true".
   *
   * @return translated "must be true"
   */
  @DefaultStringValue("must be true")
  @Key("javax.validation.constraints.AssertTrue.message")
  String javax_validation_constraints_AssertTrue_message();

  /**
   * Translated "must be less than or equal to {value}".
   *
   * @return translated "must be less than or equal to {value}"
   */
  @DefaultStringValue("must be less than or equal to {value}")
  @Key("javax.validation.constraints.DecimalMax.message")
  String javax_validation_constraints_DecimalMax_message();

  /**
   * Translated "must be greater than or equal to {value}".
   *
   * @return translated "must be greater than or equal to {value}"
   */
  @DefaultStringValue("must be greater than or equal to {value}")
  @Key("javax.validation.constraints.DecimalMin.message")
  String javax_validation_constraints_DecimalMin_message();

  /**
   * Translated "numeric value out of bounds (&lt;{integer} digits&gt;.&lt;{fraction} digits&gt;
   * expected)".
   *
   * @return translated "numeric value out of bounds (&lt;{integer} digits&gt;.&lt;{fraction}
   *         digits&gt; expected)"
   */
  @DefaultStringValue("numeric value out of bounds "
      + "(<{integer} digits>.<{fraction} digits> expected)")
  @Key("javax.validation.constraints.Digits.message")
  String javax_validation_constraints_Digits_message();

  /**
   * Translated "must be a well-formed email address".
   *
   * @return translated "must be a well-formed email address"
   */
  @DefaultStringValue("must be a well-formed email address")
  @Key("javax.validation.constraints.Email.message")
  String javax_validation_constraints_Email_message();

  /**
   * Translated "must be in the future".
   *
   * @return translated "must be in the future"
   */
  @DefaultStringValue("must be in the future")
  @Key("javax.validation.constraints.Future.message")
  String javax_validation_constraints_Future_message();

  /**
   * Translated "must be a date in the present or in the future".
   *
   * @return translated "must be a date in the present or in the future"
   */
  @DefaultStringValue("must be a date in the present or in the future")
  @Key("javax.validation.constraints.FutureOrPresent.message")
  String javax_validation_constraints_FutureOrPresent_message();

  /**
   * Translated "must be less than or equal to {value}".
   *
   * @return translated "must be less than or equal to {value}"
   */
  @DefaultStringValue("must be less than or equal to {value}")
  @Key("javax.validation.constraints.Max.message")
  String javax_validation_constraints_Max_message();

  /**
   * Translated "must be greater than or equal to {value}".
   *
   * @return translated "must be greater than or equal to {value}"
   */
  @DefaultStringValue("must be greater than or equal to {value}")
  @Key("javax.validation.constraints.Min.message")
  String javax_validation_constraints_Min_message();

  /**
   * Translated "must be less than 0".
   *
   * @return translated "must be less than 0"
   */
  @DefaultStringValue("must be less than 0")
  @Key("javax.validation.constraints.Negative.message")
  String javax_validation_constraints_Negative_message();

  /**
   * Translated "must be less than or equal to 0".
   *
   * @return translated "must be less than or equal to 0"
   */
  @DefaultStringValue("must be less than or equal to 0")
  @Key("javax.validation.constraints.NegativeOrZero.message")
  String javax_validation_constraints_NegativeOrZero_message();

  /**
   * Translated "must not be empty".
   *
   * @return translated "must not be empty"
   */
  @DefaultStringValue("must not be empty")
  @Key("javax.validation.constraints.NotBlank.message")
  String javax_validation_constraints_NotBlank_message();

  /**
   * Translated "must not be empty".
   *
   * @return translated "must not be empty"
   */
  @DefaultStringValue("must not be empty")
  @Key("javax.validation.constraints.NotEmpty.message")
  String javax_validation_constraints_NotEmpty_message();

  /**
   * Translated "must not be null".
   *
   * @return translated "must not be null"
   */
  @DefaultStringValue("must not be null")
  @Key("javax.validation.constraints.NotNull.message")
  String javax_validation_constraints_NotNull_message();

  /**
   * Translated "must be null".
   *
   * @return translated "must be null"
   */
  @DefaultStringValue("must be null")
  @Key("javax.validation.constraints.Null.message")
  String javax_validation_constraints_Null_message();

  /**
   * Translated "must be in the past".
   *
   * @return translated "must be in the past"
   */
  @DefaultStringValue("must be in the past")
  @Key("javax.validation.constraints.Past.message")
  String javax_validation_constraints_Past_message();

  /**
   * Translated "must be a date in the past or in the present".
   *
   * @return translated "must be a date in the past or in the present"
   */
  @DefaultStringValue("must be a date in the past or in the present")
  @Key("javax.validation.constraints.PastOrPresent.message")
  String javax_validation_constraints_PastOrPresent_message();

  /**
   * Translated "must match \"{regexp}\"".
   *
   * @return translated "must match \"{regexp}\""
   */
  @DefaultStringValue("must match \"{regexp}\"")
  @Key("javax.validation.constraints.Pattern.message")
  String javax_validation_constraints_Pattern_message();

  /**
   * Translated "must be greater than 0".
   *
   * @return translated "must be greater than 0"
   */
  @DefaultStringValue("must be greater than 0")
  @Key("javax.validation.constraints.Positive.message")
  String javax_validation_constraints_Positive_message();

  /**
   * Translated "must be greater than or equal to 0".
   *
   * @return translated "must be greater than or equal to 0"
   */
  @DefaultStringValue("must be greater than or equal to 0")
  @Key("javax.validation.constraints.PositiveOrZero.message")
  String javax_validation_constraints_PositiveOrZero_message();

  /**
   * Translated "size must be between {min} and {max}".
   *
   * @return translated "size must be between {min} and {max}"
   */
  @DefaultStringValue("size must be between {min} and {max}")
  @Key("javax.validation.constraints.Size.message")
  String javax_validation_constraints_Size_message();

  /**
   * Translated "invalid credit card number".
   *
   * @return translated "invalid credit card number"
   */
  @DefaultStringValue("invalid credit card number")
  @Key("org.hibernate.validator.constraints.CreditCardNumber.message")
  String org_hibernate_validator_constraints_CreditCardNumber_message();

  /**
   * Translated "invalid currency (must be one of {value})".
   *
   * @return translated "invalid currency (must be one of {value})"
   */
  @DefaultStringValue("invalid currency (must be one of {value})")
  @Key("org.hibernate.validator.constraints.Currency.message")
  String org_hibernate_validator_constraints_Currency_message();

  /**
   * Translated "invalid {type} barcode".
   *
   * @return translated "invalid {type} barcode"
   */
  @DefaultStringValue("invalid {type} barcode")
  @Key("org.hibernate.validator.constraints.EAN.message")
  String org_hibernate_validator_constraints_EAN_message();

  /**
   * Translated "not a well-formed email address".
   *
   * @return translated "not a well-formed email address"
   */
  @DefaultStringValue("not a well-formed email address")
  @Key("org.hibernate.validator.constraints.Email.message")
  String org_hibernate_validator_constraints_Email_message();

  /**
   * Translated "invalid ISBN".
   *
   * @return translated "invalid ISBN"
   */
  @DefaultStringValue("invalid ISBN")
  @Key("org.hibernate.validator.constraints.ISBN.message")
  String org_hibernate_validator_constraints_ISBN_message();

  /**
   * Translated "length must be between {min} and {max}".
   *
   * @return translated "length must be between {min} and {max}"
   */
  @DefaultStringValue("length must be between {min} and {max}")
  @Key("org.hibernate.validator.constraints.Length.message")
  String org_hibernate_validator_constraints_Length_message();

  /**
   * Translated "length must be between {min} and {max}".
   *
   * @return translated "length must be between {min} and {max}"
   */
  @DefaultStringValue("length must be between {min} and {max}")
  @Key("org.hibernate.validator.constraints.CodePointLength.message")
  String org_hibernate_validator_constraints_CodePointLength_message();

  /**
   * Translated "The check digit for {value} is invalid, Luhn Modulo 10 checksum failed".
   *
   * @return translated "The check digit for {value} is invalid, Luhn Modulo 10 checksum failed"
   */
  @DefaultStringValue("The check digit for {value} is invalid, " + "Luhn Modulo 10 checksum failed")
  @Key("org.hibernate.validator.constraints.LuhnCheck.message")
  String org_hibernate_validator_constraints_LuhnCheck_message();

  /**
   * Translated "The check digit for {value} is invalid, Modulo 10 checksum failed".
   *
   * @return translated "The check digit for {value} is invalid, Modulo 10 checksum failed"
   */
  @DefaultStringValue("The check digit for {value} is invalid, Modulo 10 checksum failed")
  @Key("org.hibernate.validator.constraints.Mod10Check.message")
  String org_hibernate_validator_constraints_Mod10Check_message();

  /**
   * Translated "The check digit for {value} is invalid, Modulo 11 checksum failed".
   *
   * @return translated "The check digit for {value} is invalid, Modulo 11 checksum failed"
   */
  @DefaultStringValue("The check digit for {value} is invalid, Modulo 11 checksum failed")
  @Key("org.hibernate.validator.constraints.Mod11Check.message")
  String org_hibernate_validator_constraints_Mod11Check_message();

  /**
   * Translated "The check digit for {value} is invalid, {modType} checksum failed".
   *
   * @return translated "The check digit for {value} is invalid, {modType} checksum failed"
   */
  @DefaultStringValue("The check digit for {value} is invalid, {modType} " + "checksum failed")
  @Key("org.hibernate.validator.constraints.ModCheck.message")
  String org_hibernate_validator_constraints_ModCheck_message();

  /**
   * Translated "may not be empty".
   *
   * @return translated "may not be empty"
   */
  @DefaultStringValue("may not be empty")
  @Key("org.hibernate.validator.constraints.NotBlank.message")
  String org_hibernate_validator_constraints_NotBlank_message();

  /**
   * Translated "may not be empty".
   *
   * @return translated "may not be empty"
   */
  @DefaultStringValue("may not be empty")
  @Key("org.hibernate.validator.constraints.NotEmpty.message")
  String org_hibernate_validator_constraints_NotEmpty_message();

  /**
   * Translated "script expression "{script}" didn't evaluate to true".
   *
   * @return translated "script expression "{script}" didn't evaluate to true"
   */
  @DefaultStringValue("script expression \"{script}\" didn't evaluate to true")
  @Key("org.hibernate.validator.constraints.ParametersScriptAssert.message")
  String org_hibernate_validator_constraints_ParametersScriptAssert_message();

  /**
   * Translated "must be between {min} and {max}".
   *
   * @return translated "must be between {min} and {max}"
   */
  @DefaultStringValue("must be between {min} and {max}")
  @Key("org.hibernate.validator.constraints.Range.message")
  String org_hibernate_validator_constraints_Range_message();

  /**
   * Translated "may have unsafe html content".
   *
   * @return translated "may have unsafe html content"
   */
  @DefaultStringValue("may have unsafe html content")
  @Key("org.hibernate.validator.constraints.SafeHtml.message")
  String org_hibernate_validator_constraints_SafeHtml_message();

  /**
   * Translated "script expression \"{script}\" didn't evaluate to true".
   *
   * @return translated "script expression \"{script}\" didn't evaluate to true"
   */
  @DefaultStringValue("script expression \"{script}\" didn't evaluate to true")
  @Key("org.hibernate.validator.constraints.ScriptAssert.message")
  String org_hibernate_validator_constraints_ScriptAssert_message();

  /**
   * Translated "must only contain unique elements".
   *
   * @return translated "must only contain unique elements"
   */
  @DefaultStringValue("must only contain unique elements")
  @Key("org.hibernate.validator.constraints.UniqueElements.message")
  String org_hibernate_validator_constraints_UniqueElements_message();

  /**
   * Translated "must be a valid URL".
   *
   * @return translated "must be a valid URL"
   */
  @DefaultStringValue("must be a valid URL")
  @Key("org.hibernate.validator.constraints.URL.message")
  String org_hibernate_validator_constraints_URL_message();

  /**
   * Translated "invalid Brazilian corporate taxpayer registry number (CNPJ)".
   *
   * @return translated "invalid Brazilian corporate taxpayer registry number (CNPJ)"
   */
  @DefaultStringValue("invalid Brazilian corporate taxpayer registry number (CNPJ)")
  @Key("org.hibernate.validator.constraints.br.CNPJ.message")
  String org_hibernate_validator_constraints_br_CNPJ_message();

  /**
   * Translated "invalid Brazilian individual taxpayer registry number (CPF)".
   *
   * @return translated "invalid Brazilian individual taxpayer registry number (CPF)"
   */
  @DefaultStringValue("invalid Brazilian individual taxpayer registry number (CPF)")
  @Key("org.hibernate.validator.constraints.br.CPF.message")
  String org_hibernate_validator_constraints_br_CPF_message();

  /**
   * Translated "invalid Brazilian Voter ID card number".
   *
   * @return translated "invalid Brazilian Voter ID card number"
   */
  @DefaultStringValue("invalid Brazilian Voter ID card number")
  @Key("org.hibernate.validator.constraints.br.TituloEleitoral.message")
  String org_hibernate_validator_constraints_br_TituloEleitoral_message();

  /**
   * Translated "Invalid Polish Taxpayer Identification Number (REGON)".
   *
   * @return translated "Invalid Polish Taxpayer Identification Number (REGON)"
   */
  @DefaultStringValue("Invalid Polish Taxpayer Identification Number (REGON)")
  @Key("org.hibernate.validator.constraints.pl.REGON.message")
  String org_hibernate_validator_constraints_pl_REGON_message();

  /**
   * Translated "Invalid VAT Identification Number (NIP)".
   *
   * @return translated "Invalid VAT Identification Number (NIP)"
   */
  @DefaultStringValue("Invalid VAT Identification Number (NIP)")
  @Key("org.hibernate.validator.constraints.pl.NIP.message")
  String org_hibernate_validator_constraints_pl_NIP_message();

  /**
   * Translated "Invalid Polish National Identification Number (PESEL)".
   *
   * @return translated "Invalid Polish National Identification Number (PESEL)"
   */
  @DefaultStringValue("Invalid Polish National Identification Number (PESEL)")
  @Key("org.hibernate.validator.constraints.pl.PESEL.message")
  String org_hibernate_validator_constraints_pl_PESEL_message();

  /**
   * Translated "must be shorter than${inclusive == true ? ' or equal to' : ''}${days == 0 ? '' :
   * days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' 1 hour' : ' '
   * += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' ' += minutes += '
   * minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' += seconds += '
   * seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis += ' millis'}${nanos ==
   * 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}".
   *
   * @return translated "must be shorter than${inclusive == true ? ' or equal to' : ''}${days == 0 ?
   *         '' : days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' 1
   *         hour' : ' ' += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' '
   *         += minutes += ' minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' +=
   *         seconds += ' seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis +=
   *         ' millis'}${nanos == 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}"
   */
  @DefaultStringValue("must be shorter than${inclusive == true ? ' or equal to' : ''}${days == 0 ?"
      + " '' : days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' "
      + "1 hour' : ' ' += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' ' "
      + "+= minutes += ' minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' "
      + "+= seconds += ' seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis "
      + "+= ' millis'}${nanos == 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}")
  @Key("org.hibernate.validator.constraints.time.DurationMax.message")
  String org_hibernate_validator_constraints_time_DurationMax_message();

  /**
   * Translated "must be longer than${inclusive == true ? ' or equal to' : ''}${days == 0 ? '' :
   * days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' 1 hour' : ' '
   * += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' ' += minutes += '
   * minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' += seconds += '
   * seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis += ' millis'}${nanos ==
   * 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}".
   *
   * @return translated "must be longer than${inclusive == true ? ' or equal to' : ''}${days == 0 ?
   *         '' : days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' 1
   *         hour' : ' ' += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : ' '
   *         += minutes += ' minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' +=
   *         seconds += ' seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis +=
   *         ' millis'}${nanos == 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}"
   */
  @DefaultStringValue("must be longer than${inclusive == true ? ' or equal to' : ''}${days == 0 ? "
      + "'' : days == 1 ? ' 1 day' : ' ' += days += ' days'}${hours == 0 ? '' : hours == 1 ? ' "
      + "1 hour' : ' ' += hours += ' hours'}${minutes == 0 ? '' : minutes == 1 ? ' 1 minute' : "
      + "' ' += minutes += ' minutes'}${seconds == 0 ? '' : seconds == 1 ? ' 1 second' : ' ' "
      + "+= seconds += ' seconds'}${millis == 0 ? '' : millis == 1 ? ' 1 milli' : ' ' += millis "
      + "+= ' millis'}${nanos == 0 ? '' : nanos == 1 ? ' 1 nano' : ' ' += nanos += ' nanos'}")
  @Key("org.hibernate.validator.constraints.time.DurationMin.message")
  String org_hibernate_validator_constraints_time_DurationMin_message();
}
