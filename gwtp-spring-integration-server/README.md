# gwtp-spring-integration-server
Basic functionality to integrate gwtp with spring, server part.

It includes

* authentication integration with spring security
* user session handling
* validation handling and report to client
* rest callback implementations handling errors from server
* expose some interfaces to client

Maven integration
-----------------

The dependency itself for GWT-Projects:

```
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwtp-spring-integration-server</artifactId>
      <version>1.0.0</version>
    </dependency>
```
