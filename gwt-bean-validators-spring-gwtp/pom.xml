<?xml version="1.0" encoding="UTF-8"?>
<!-- Licensed to the Apache Software Foundation (ASF) under one or more contributor 
	license agreements. See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership. The ASF licenses this file to 
	You under the Apache License, Version 2.0 (the "License"); you may not use 
	this file except in compliance with the License. You may obtain a copy of 
	the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>de.knightsoft-net</groupId>
		<artifactId>gwt-bean-validators-parent</artifactId>
		<version>1.0.0</version>
	</parent>
	<artifactId>gwt-bean-validators-spring-gwtp</artifactId>
	<packaging>jar</packaging>
	<name>GWT Bean Validators - REST services using spring and gwtp</name>
	<description>
		The GWT Bean Validators is a collection of JSR-303/JSR-349/JSR 380 bean validators. It can be used on server
		and with the help of GWT even on client side. This packages contains rest services and validator
		for phone numbers based on gwtp-rest dispatcher and spring to reduce client side code.
	</description>
	<url>https://gitlab.com/ManfredTremmel/gwt-bean-validators/tree/master/gwt-bean-validators-spring-gwtp</url>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<prerequisites>
		<maven>3.0.0</maven>
	</prerequisites>

	<developers>
		<developer>
			<name>Manfred Tremmel</name>
			<id>ManfredTremmel</id>
			<email>Manfred.Tremmel@iiv.de</email>
			<roles>
				<role>Java Developer</role>
			</roles>
		</developer>
	</developers>

	<dependencies>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>gwt-bean-validators</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>gwtp-dynamic-navigation</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-dispatch-rest</artifactId>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform.extensions</groupId>
			<artifactId>dispatch-rest-delegates</artifactId>
		</dependency>
	</dependencies>

	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
			</resource>
			<resource>
				<!-- the java source files must be on the classpath during gwt:compile -->
				<directory>src/main/java</directory>
			</resource>
			<resource>
				<!-- the java source files that override/emulate server-side classes 
					in the client must be on the classpath during gwt:compile -->
				<directory>src/main/super</directory>
			</resource>
		</resources>
		<testResources>
			<testResource>
				<directory>src/test/resources</directory>
			</testResource>
		</testResources>
		<plugins>
			<!-- GWT Maven Plugin -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>gwt-maven-plugin</artifactId>
				<!-- Plugin configuration. There are many available options, see gwt-maven-plugin 
					documentation at codehaus.org -->
				<configuration>
					<modules>
						<module>de.knightsoftnet.validators.GwtBeanValidatorsSpringGwtp</module>
					</modules>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
</project>
