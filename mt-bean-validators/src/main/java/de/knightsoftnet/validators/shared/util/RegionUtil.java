/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CountryEnum;

import org.apache.commons.lang3.StringUtils;

/**
 * Taxnumber Util, helper tools for tax numbers.
 *
 * @author Manfred Tremmel
 *
 */
public class RegionUtil {

  /**
   * get region for country code and postal code. Germany only at the moment!
   *
   * @param pcountryCode country code
   * @param ppostal code postal code
   * @return corresponding region or null
   */
  public static String regionForCountryAndPostalCode(final CountryEnum pcountryCode,
      final String ppostalCode) {
    if (pcountryCode == null || StringUtils.isEmpty(ppostalCode)) {
      return null;
    }
    if (pcountryCode == CountryEnum.DE && ppostalCode.matches("^[0-9]{5}$")) {
      final int postalCodeNumeric = Integer.parseInt(ppostalCode);
      if (postalCodeNumeric >= 1001 && postalCodeNumeric < 1936 //
          || postalCodeNumeric >= 2601 && postalCodeNumeric <= 2999 //
          || postalCodeNumeric >= 4001 && postalCodeNumeric <= 4579 //
          || postalCodeNumeric >= 4641 && postalCodeNumeric <= 4889 //
          || postalCodeNumeric >= 7951 && postalCodeNumeric <= 7952 //
          || postalCodeNumeric >= 7982 && postalCodeNumeric <= 7982 //
          || postalCodeNumeric >= 8001 && postalCodeNumeric <= 9669) {
        return "Sachsen";
      }
      if (postalCodeNumeric >= 1941 && postalCodeNumeric <= 1998 //
          || postalCodeNumeric >= 3001 && postalCodeNumeric <= 3253 //
          || postalCodeNumeric >= 4891 && postalCodeNumeric <= 4938 //
          || postalCodeNumeric >= 14401 && postalCodeNumeric <= 16949 //
          || postalCodeNumeric >= 17261 && postalCodeNumeric <= 17291 //
          || postalCodeNumeric >= 19307 && postalCodeNumeric <= 19357) {
        return "Brandenburg";
      }
      if (postalCodeNumeric >= 4581 && postalCodeNumeric <= 4639 //
          || postalCodeNumeric >= 6551 && postalCodeNumeric <= 6578 //
          || postalCodeNumeric >= 7301 && postalCodeNumeric <= 7950 //
          || postalCodeNumeric >= 7952 && postalCodeNumeric <= 7980 //
          || postalCodeNumeric >= 7985 && postalCodeNumeric <= 7989 //
          || postalCodeNumeric >= 36401 && postalCodeNumeric <= 36469 //
          || postalCodeNumeric >= 37301 && postalCodeNumeric <= 37359 //
          || postalCodeNumeric >= 96501 && postalCodeNumeric <= 96529 //
          || postalCodeNumeric >= 98501 && postalCodeNumeric <= 99998) {
        return "Thüringen";
      }
      if (postalCodeNumeric >= 6001 && postalCodeNumeric <= 6548 //
          || postalCodeNumeric >= 6601 && postalCodeNumeric <= 6928 //
          || postalCodeNumeric >= 19271 && postalCodeNumeric <= 19273 //
          || postalCodeNumeric >= 29401 && postalCodeNumeric <= 29416 //
          || postalCodeNumeric >= 38481 && postalCodeNumeric <= 38489 //
          || postalCodeNumeric >= 38801 && postalCodeNumeric <= 39649) {
        return "Sachsen-Anhalt";
      }
      if (postalCodeNumeric >= 10001 && postalCodeNumeric <= 14330) {
        return "Berlin";
      }
      if (postalCodeNumeric >= 17001 && postalCodeNumeric <= 17259 //
          || postalCodeNumeric >= 17301 && postalCodeNumeric <= 19260 //
          || postalCodeNumeric >= 19273 && postalCodeNumeric <= 19306 //
          || postalCodeNumeric >= 19357 && postalCodeNumeric <= 19417 //
          || postalCodeNumeric >= 23921 && postalCodeNumeric <= 23999) {
        return "Mecklenburg-Vorpommern";
      }
      if (postalCodeNumeric >= 20001 && postalCodeNumeric <= 21170 //
          || postalCodeNumeric >= 22001 && postalCodeNumeric <= 22786 //
          || postalCodeNumeric >= 27499 && postalCodeNumeric <= 27499) {
        return "Hamburg";
      }
      if (postalCodeNumeric >= 21202 && postalCodeNumeric <= 21449 //
          || postalCodeNumeric >= 21601 && postalCodeNumeric <= 21789 //
          || postalCodeNumeric >= 26001 && postalCodeNumeric <= 27478 //
          || postalCodeNumeric >= 27607 && postalCodeNumeric <= 27809 //
          || postalCodeNumeric >= 28784 && postalCodeNumeric <= 29399 //
          || postalCodeNumeric >= 29431 && postalCodeNumeric <= 31868 //
          || postalCodeNumeric >= 34331 && postalCodeNumeric <= 34353 //
          || postalCodeNumeric >= 37001 && postalCodeNumeric <= 37194 //
          || postalCodeNumeric >= 37197 && postalCodeNumeric <= 37199 //
          || postalCodeNumeric >= 37401 && postalCodeNumeric <= 37649 //
          || postalCodeNumeric >= 37689 && postalCodeNumeric <= 37691 //
          || postalCodeNumeric >= 37697 && postalCodeNumeric <= 38479 //
          || postalCodeNumeric >= 38501 && postalCodeNumeric <= 38729 //
          || postalCodeNumeric >= 48442 && postalCodeNumeric <= 48465 //
          || postalCodeNumeric >= 48478 && postalCodeNumeric <= 48480 //
          || postalCodeNumeric >= 48486 && postalCodeNumeric <= 48488 //
          || postalCodeNumeric >= 48497 && postalCodeNumeric <= 48531 //
          || postalCodeNumeric >= 49001 && postalCodeNumeric <= 49459 //
          || postalCodeNumeric >= 49551 && postalCodeNumeric <= 49849) {
        return "Niedersachsen";
      }
      if (postalCodeNumeric >= 22801 && postalCodeNumeric <= 23919 //
          || postalCodeNumeric >= 24001 && postalCodeNumeric <= 25999 //
          || postalCodeNumeric >= 27483 && postalCodeNumeric <= 27498) {
        return "Schleswig-Holstein";
      }
      if (postalCodeNumeric >= 27501 && postalCodeNumeric <= 27580 //
          || postalCodeNumeric >= 28001 && postalCodeNumeric <= 28779) {
        return "Bremen";
      }
      if (postalCodeNumeric >= 32001 && postalCodeNumeric <= 33829 //
          || postalCodeNumeric >= 34401 && postalCodeNumeric <= 34439 //
          || postalCodeNumeric >= 37651 && postalCodeNumeric <= 37688 //
          || postalCodeNumeric >= 37692 && postalCodeNumeric <= 37696 //
          || postalCodeNumeric >= 40001 && postalCodeNumeric <= 48432 //
          || postalCodeNumeric >= 48466 && postalCodeNumeric <= 48477 //
          || postalCodeNumeric >= 48481 && postalCodeNumeric <= 48485 //
          || postalCodeNumeric >= 48489 && postalCodeNumeric <= 48496 //
          || postalCodeNumeric >= 48541 && postalCodeNumeric <= 48739 //
          || postalCodeNumeric >= 49461 && postalCodeNumeric <= 49549 //
          || postalCodeNumeric >= 50101 && postalCodeNumeric <= 51597 //
          || postalCodeNumeric >= 51601 && postalCodeNumeric <= 53359 //
          || postalCodeNumeric >= 53581 && postalCodeNumeric <= 53604 //
          || postalCodeNumeric >= 53621 && postalCodeNumeric <= 53949 //
          || postalCodeNumeric >= 57001 && postalCodeNumeric <= 57489 //
          || postalCodeNumeric >= 58001 && postalCodeNumeric <= 59969) {
        return "Nordrhein-Westfalen";
      }
      if (postalCodeNumeric >= 34001 && postalCodeNumeric <= 34329 //
          || postalCodeNumeric >= 34355 && postalCodeNumeric <= 34399 //
          || postalCodeNumeric >= 34441 && postalCodeNumeric <= 36399 //
          || postalCodeNumeric >= 37194 && postalCodeNumeric <= 37195 //
          || postalCodeNumeric >= 37201 && postalCodeNumeric <= 37299 //
          || postalCodeNumeric >= 55240 && postalCodeNumeric <= 55252 //
          || postalCodeNumeric >= 60001 && postalCodeNumeric <= 63699 //
          || postalCodeNumeric >= 64201 && postalCodeNumeric <= 65326 //
          || postalCodeNumeric >= 65327 && postalCodeNumeric <= 65556 //
          || postalCodeNumeric >= 65583 && postalCodeNumeric <= 65620 //
          || postalCodeNumeric >= 65627 && postalCodeNumeric <= 65627 //
          || postalCodeNumeric >= 65701 && postalCodeNumeric <= 65936 //
          || postalCodeNumeric >= 68501 && postalCodeNumeric <= 68519 //
          || postalCodeNumeric >= 68601 && postalCodeNumeric <= 68649 //
          || postalCodeNumeric >= 69235 && postalCodeNumeric <= 69239 //
          || postalCodeNumeric >= 69430 && postalCodeNumeric <= 69434 //
          || postalCodeNumeric >= 69479 && postalCodeNumeric <= 69488 //
          || postalCodeNumeric >= 69503 && postalCodeNumeric <= 69509 //
          || postalCodeNumeric >= 69515 && postalCodeNumeric <= 69518) {
        return "Hessen";
      }
      if (postalCodeNumeric >= 51598 && postalCodeNumeric <= 51598 //
          || postalCodeNumeric >= 53401 && postalCodeNumeric <= 53579 //
          || postalCodeNumeric >= 53614 && postalCodeNumeric <= 53619 //
          || postalCodeNumeric >= 54181 && postalCodeNumeric <= 55239 //
          || postalCodeNumeric >= 55253 && postalCodeNumeric <= 56869 //
          || postalCodeNumeric >= 57501 && postalCodeNumeric <= 57648 //
          || postalCodeNumeric >= 65326 && postalCodeNumeric <= 65326 //
          || postalCodeNumeric >= 65558 && postalCodeNumeric <= 65582 //
          || postalCodeNumeric >= 65621 && postalCodeNumeric <= 65626 //
          || postalCodeNumeric >= 65629 && postalCodeNumeric <= 65629 //
          || postalCodeNumeric >= 66461 && postalCodeNumeric <= 66509 //
          || postalCodeNumeric >= 66841 && postalCodeNumeric <= 67829 //
          || postalCodeNumeric >= 76711 && postalCodeNumeric <= 76891) {
        return "Rheinland-Pfalz";
      }
      if (postalCodeNumeric >= 63701 && postalCodeNumeric <= 63928 //
          || postalCodeNumeric >= 63930 && postalCodeNumeric <= 63939 //
          || postalCodeNumeric >= 74594 && postalCodeNumeric <= 74594 //
          || postalCodeNumeric >= 80001 && postalCodeNumeric <= 87789 //
          || postalCodeNumeric >= 88101 && postalCodeNumeric <= 88179 //
          || postalCodeNumeric >= 89087 && postalCodeNumeric <= 89087 //
          || postalCodeNumeric >= 89201 && postalCodeNumeric <= 89449 //
          || postalCodeNumeric >= 90001 && postalCodeNumeric <= 96489 //
          || postalCodeNumeric >= 97001 && postalCodeNumeric <= 97859 //
          || postalCodeNumeric >= 97888 && postalCodeNumeric <= 97892 //
          || postalCodeNumeric >= 97896 && postalCodeNumeric <= 97896 //
          || postalCodeNumeric >= 97901 && postalCodeNumeric <= 97909) {
        return "Bayern";
      }
      if (postalCodeNumeric >= 63928 && postalCodeNumeric <= 63928 //
          || postalCodeNumeric >= 68001 && postalCodeNumeric <= 68312 //
          || postalCodeNumeric >= 68520 && postalCodeNumeric <= 68549 //
          || postalCodeNumeric >= 68701 && postalCodeNumeric <= 69234 //
          || postalCodeNumeric >= 69240 && postalCodeNumeric <= 69429 //
          || postalCodeNumeric >= 69435 && postalCodeNumeric <= 69469 //
          || postalCodeNumeric >= 69489 && postalCodeNumeric <= 69502 //
          || postalCodeNumeric >= 69510 && postalCodeNumeric <= 69514 //
          || postalCodeNumeric >= 70001 && postalCodeNumeric <= 74592 //
          || postalCodeNumeric >= 74594 && postalCodeNumeric <= 76709 //
          || postalCodeNumeric >= 77601 && postalCodeNumeric <= 79879 //
          || postalCodeNumeric >= 88001 && postalCodeNumeric <= 88099 //
          || postalCodeNumeric >= 88181 && postalCodeNumeric <= 89085 //
          || postalCodeNumeric >= 89090 && postalCodeNumeric <= 89198 //
          || postalCodeNumeric >= 89501 && postalCodeNumeric <= 89619 //
          || postalCodeNumeric >= 97861 && postalCodeNumeric <= 97877 //
          || postalCodeNumeric >= 97893 && postalCodeNumeric <= 97896 //
          || postalCodeNumeric >= 97897 && postalCodeNumeric <= 97900 //
          || postalCodeNumeric >= 97911 && postalCodeNumeric <= 97999) {
        return "Baden-Württemberg";
      }
      if (postalCodeNumeric >= 66001 && postalCodeNumeric <= 66459 //
          || postalCodeNumeric >= 66511 && postalCodeNumeric <= 66839) {
        return "Saarland";
      }
    }
    return null;
  }
}
