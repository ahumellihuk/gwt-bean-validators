/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.util;

import de.knightsoftnet.validators.shared.data.CountryEnum;

import org.apache.commons.lang3.StringUtils;

/**
 * Region Util, get region for country and postal code.
 *
 * @author Manfred Tremmel
 *
 */
public class TaxNumberUtil {

  /**
   * get region for country code and postal code. Germany only at the moment!
   *
   * @param ptaxNumber tax number
   * @param pcountryCode country code
   * @param ppostal code postal code
   * @return corresponding region or null
   */
  public static String regionTaxToNational(final String ptaxNumber, final CountryEnum pcountryCode,
      final String ppostalCode) {
    final String region = RegionUtil.regionForCountryAndPostalCode(pcountryCode, ppostalCode);
    switch (StringUtils.defaultString(region)) {
      case "Baden-Württemberg":
        return badenWuertembergTaxToNational(ptaxNumber);
      case "Bayern":
        return bayernTaxToNational(ptaxNumber);
      case "Berlin":
        return berlinTaxToNational(ptaxNumber);
      case "Brandenburg":
        return brandenburgTaxToNational(ptaxNumber);
      case "Bremen":
        return bremenTaxToNational(ptaxNumber);
      case "Hamburg":
        return hamburgTaxToNational(ptaxNumber);
      case "Hessen":
        return hessenTaxToNational(ptaxNumber);
      case "Mecklenburg-Vorpommern":
        return mecklenburgVorpommernTaxToNational(ptaxNumber);
      case "Niedersachsen":
        return niedersachsenTaxToNational(ptaxNumber);
      case "Nordrhein-Westfalen":
        return nordrheinWestfalenTaxToNational(ptaxNumber);
      case "Rheinland-Pfalz":
        return rheinlandPfalzTaxToNational(ptaxNumber);
      case "Saarland":
        return saarlandTaxToNational(ptaxNumber);
      case "Sachsen":
        return sachsenTaxToNational(ptaxNumber);
      case "Sachsen-Anhalt":
        return sachsenAnhaltTaxToNational(ptaxNumber);
      case "Schleswig-Holstein":
        return schleswigHolsteinTaxToNational(ptaxNumber);
      case "Thüringen":
        return thueringenTaxToNational(ptaxNumber);
      default:
        return ptaxNumber;
    }
  }

  /**
   * convert baden wuertemberg tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String badenWuertembergTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{5}/[0-9]{5}$")) {
      return "28" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 2), '/');
    }
    return taxNumber;
  }

  /**
   * convert bayern tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String bayernTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{3}/[0-9]{3}/[0-9]{5}$")) {
      return "9" + StringUtils.substring(taxNumber, 0, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert berlin tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String berlinTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "11" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), '/');
    }
    return taxNumber;
  }

  /**
   * convert brandenburg tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String brandenburgTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^0[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "30" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert bremen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String bremenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2} [0-9]{3} [0-9]{5}$")) {
      return "24" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), ' ');
    }
    return taxNumber;
  }

  /**
   * convert hamburg tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String hamburgTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "22" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), '/');
    }
    return taxNumber;
  }

  /**
   * convert hessen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String hessenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^0[0-9]{2} [0-9]{3} [0-9]{5}$")) {
      return "26" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), ' ');
    }
    return taxNumber;
  }

  /**
   * convert Mecklenburg-Vorpommern tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String mecklenburgVorpommernTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^0[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "40" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert niedersachsen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String niedersachsenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "23" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), '/');
    }
    return taxNumber;
  }

  /**
   * convert Nordrhein-Westfalen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String nordrheinWestfalenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{3}/[0-9]{4}/[0-9]{4}$")) {
      return "5" + StringUtils.substring(taxNumber, 0, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert Rheinland-Pfalz tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String rheinlandPfalzTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "27" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), '/');
    }
    return taxNumber;
  }

  /**
   * convert Saarland tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String saarlandTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^0[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "10" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert Sachsen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String sachsenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^2[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "32" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert Sachsen-Anhalt tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String sachsenAnhaltTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^1[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "31" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }

  /**
   * convert Schleswig-Holstein tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String schleswigHolsteinTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "21" + StringUtils.substring(taxNumber, 0, 2) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 3), '/');
    }
    return taxNumber;
  }

  /**
   * convert Thüringen tax number to national format.
   *
   * @param taxNumber regional tax number
   * @return national tax number
   */
  public static String thueringenTaxToNational(final String taxNumber) {
    if (taxNumber != null && taxNumber.matches("^1[0-9]{2}/[0-9]{3}/[0-9]{5}$")) {
      return "41" + StringUtils.substring(taxNumber, 1, 3) + "0"
          + StringUtils.remove(StringUtils.substring(taxNumber, 4), '/');
    }
    return taxNumber;
  }
}
