/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.impl;

import java.time.ZonedDateTime;

import javax.validation.ConstraintValidatorContext;

/**
 * Check if a given date is minYears years ago for ZonedDateTime.
 *
 * @author Manfred Tremmel
 *
 */
public class AgeLimitCheckValidatorForZoneDateTime
    extends AbstractAgeLimitCheckValidator<ZonedDateTime> {

  @Override
  public final boolean isValid(final ZonedDateTime pvalue,
      final ConstraintValidatorContext pcontext) {
    if (pvalue == null) {
      return true;
    }
    final ZonedDateTime dateLimit = ZonedDateTime.now().minusYears(minYears).withHour(0)
        .withMinute(0).withSecond(0).withNano(0);
    return !dateLimit.isBefore(pvalue.withHour(0).withMinute(0).withSecond(0).withNano(0));
  }
}
