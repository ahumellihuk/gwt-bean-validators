package de.knightsoftnet.validators.shared.validators;

import de.knightsoftnet.validators.shared.beans.Property;

import org.hibernate.validator.internal.constraintvalidators.bv.notempty.NotEmptyValidatorForCollection;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotEmpty;

public class NotEmptyValidatorForCollectionProperty
    implements ConstraintValidator<NotEmpty, Property<? extends Collection<?>>> {

  private final NotEmptyValidatorForCollection superValidator =
      new NotEmptyValidatorForCollection();

  @Override
  public void initialize(final NotEmpty constraintAnnotation) {
    superValidator.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(final Property<? extends Collection<?>> value,
      final ConstraintValidatorContext context) {
    return superValidator.isValid(value == null ? null : value.getValue(), context);
  }
}
