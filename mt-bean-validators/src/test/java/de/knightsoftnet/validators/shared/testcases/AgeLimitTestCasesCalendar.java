/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.AgeLimitTestBeanCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * get test cases for age limit test.
 *
 * @author Manfred Tremmel
 *
 */
public class AgeLimitTestCasesCalendar {
  /**
   * get empty test bean.
   *
   * @return empty test bean
   */
  public static final AgeLimitTestBeanCalendar getEmptyTestBean() {
    return new AgeLimitTestBeanCalendar(null);
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<AgeLimitTestBeanCalendar> getCorrectTestBeans() {
    final List<AgeLimitTestBeanCalendar> correctCases = new ArrayList<>();
    final Calendar first = Calendar.getInstance();
    first.add(Calendar.YEAR, 0 - AgeLimitTestBeanCalendar.AGE_LIMIT - 1);
    correctCases.add(new AgeLimitTestBeanCalendar(first));
    final Calendar second = Calendar.getInstance();
    second.add(Calendar.YEAR, 0 - AgeLimitTestBeanCalendar.AGE_LIMIT);
    correctCases.add(new AgeLimitTestBeanCalendar(second));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<AgeLimitTestBeanCalendar> getWrongTestBeans() {
    final List<AgeLimitTestBeanCalendar> wrongCases = new ArrayList<>();
    final Calendar first = Calendar.getInstance();
    wrongCases.add(new AgeLimitTestBeanCalendar(first));
    final Calendar second = Calendar.getInstance();
    second.add(Calendar.YEAR, 0 - AgeLimitTestBeanCalendar.AGE_LIMIT);
    second.add(Calendar.DAY_OF_WEEK, 1);
    wrongCases.add(new AgeLimitTestBeanCalendar(second));
    return wrongCases;
  }
}
