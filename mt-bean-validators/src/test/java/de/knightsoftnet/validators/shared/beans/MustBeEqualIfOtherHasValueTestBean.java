/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import de.knightsoftnet.validators.shared.MustBeEqualIfOtherHasValue;
import de.knightsoftnet.validators.shared.MustNotBeEqualIfOtherHasValue;

@MustNotBeEqualIfOtherHasValue(field1 = "passwordOld", field2 = "passwordNew",
    fieldCompare = "stage", valueCompare = "2")
@MustBeEqualIfOtherHasValue(field1 = "passwordNew", field2 = "passwordNewRepeat",
    fieldCompare = "stage", valueCompare = "2")
public class MustBeEqualIfOtherHasValueTestBean {

  private final String stage;

  private final String passwordOld;

  private final String passwordNew;

  private final String passwordNewRepeat;

  /**
   * constructor initializing fields.
   *
   * @param ppasswordOld old password
   * @param ppasswordNew new password
   * @param ppasswordNewRepeat new password repeated
   */
  public MustBeEqualIfOtherHasValueTestBean(final String pstage, final String ppasswordOld,
      final String ppasswordNew, final String ppasswordNewRepeat) {
    super();
    stage = pstage;
    passwordOld = ppasswordOld;
    passwordNew = ppasswordNew;
    passwordNewRepeat = ppasswordNewRepeat;
  }

  public final String getStage() {
    return stage;
  }

  public final String getPasswordOld() {
    return passwordOld;
  }

  public final String getPasswordNew() {
    return passwordNew;
  }

  public final String getPasswordNewRepeat() {
    return passwordNewRepeat;
  }

  @Override
  public String toString() {
    return "MustBeEqualIfOtherHasValueTestBean [stage=" + stage + ", passwordOld=" + passwordOld
        + ", passwordNew=" + passwordNew + ", passwordNewRepeat=" + passwordNewRepeat + "]";
  }
}
