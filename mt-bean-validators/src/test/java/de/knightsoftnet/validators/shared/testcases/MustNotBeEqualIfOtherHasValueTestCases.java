/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.testcases;

import de.knightsoftnet.validators.shared.beans.MustBeEqualIfOtherHasValueTestBean;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * get test cases for must not be equal test.
 *
 * @author Manfred Tremmel
 *
 */
public class MustNotBeEqualIfOtherHasValueTestCases {
  /**
   * get empty test beans.
   *
   * @return empty test bean
   */
  public static final List<MustBeEqualIfOtherHasValueTestBean> getEmptyTestBeans() {
    final List<MustBeEqualIfOtherHasValueTestBean> correctCases = new ArrayList<>();
    correctCases.add(new MustBeEqualIfOtherHasValueTestBean(null, null, null, null));
    correctCases.add(new MustBeEqualIfOtherHasValueTestBean(StringUtils.EMPTY, StringUtils.EMPTY,
        StringUtils.EMPTY, StringUtils.EMPTY));
    return correctCases;
  }

  /**
   * get correct test beans.
   *
   * @return correct test beans
   */
  public static final List<MustBeEqualIfOtherHasValueTestBean> getCorrectTestBeans() {
    final List<MustBeEqualIfOtherHasValueTestBean> correctCases = new ArrayList<>();
    correctCases.add(new MustBeEqualIfOtherHasValueTestBean("2", "old", "filled", "filled"));
    return correctCases;
  }

  /**
   * get test beans with wrong values, but no relevant state.
   *
   * @return correct test beans
   */
  public static final List<MustBeEqualIfOtherHasValueTestBean> getWrongTestBeansWithOtherState() {
    final List<MustBeEqualIfOtherHasValueTestBean> correctCases = new ArrayList<>();
    correctCases.add(new MustBeEqualIfOtherHasValueTestBean("1", "old", "old", "old"));
    return correctCases;
  }

  /**
   * get wrong test beans.
   *
   * @return wrong test beans
   */
  public static final List<MustBeEqualIfOtherHasValueTestBean> getWrongTestBeans() {
    final List<MustBeEqualIfOtherHasValueTestBean> wrongCases = new ArrayList<>();
    wrongCases.add(new MustBeEqualIfOtherHasValueTestBean("2", "old", "old", "old"));
    return wrongCases;
  }
}
