/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.shared.beans;

import java.util.List;

import javax.validation.constraints.NotEmpty;

public class HibernateNotEmptyBug11TestBean {

  @NotEmpty
  public final Property<String> value;

  @NotEmpty
  public final Property<List<Integer>> list;

  public HibernateNotEmptyBug11TestBean(final String pvalue, final List<Integer> list) {
    super();
    value = new Property<>(pvalue);
    this.list = new Property<>(list);
  }

  public final Property<String> getValue() {
    return value;
  }

  public final Property<List<Integer>> getList() {
    return list;
  }

  @Override
  public String toString() {
    return "HibernateNotEmptyBug11TestBean [value=" + value.getValue() + ", list=" + list.getValue()
        + "]";
  }
}
