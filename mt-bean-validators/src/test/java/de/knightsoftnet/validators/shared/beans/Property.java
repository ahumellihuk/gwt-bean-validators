package de.knightsoftnet.validators.shared.beans;

public class Property<T> {

  private T value;

  public Property() {
    super();
  }

  public Property(final T value) {
    super();
    this.value = value;
  }

  public T getValue() {
    return value;
  }

  public void setValue(final T value) {
    this.value = value;
  }
}
