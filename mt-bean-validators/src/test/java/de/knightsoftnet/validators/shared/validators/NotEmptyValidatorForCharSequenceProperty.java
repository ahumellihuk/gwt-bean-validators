package de.knightsoftnet.validators.shared.validators;

import de.knightsoftnet.validators.shared.beans.Property;

import org.hibernate.validator.internal.constraintvalidators.bv.notempty.NotEmptyValidatorForCharSequence;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotEmpty;

public class NotEmptyValidatorForCharSequenceProperty
    implements ConstraintValidator<NotEmpty, Property<? extends CharSequence>> {

  private final NotEmptyValidatorForCharSequence superValidator =
      new NotEmptyValidatorForCharSequence();

  @Override
  public void initialize(final NotEmpty constraintAnnotation) {
    superValidator.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(final Property<? extends CharSequence> value,
      final ConstraintValidatorContext context) {
    return superValidator.isValid(value == null ? null : value.getValue(), context);
  }
}
