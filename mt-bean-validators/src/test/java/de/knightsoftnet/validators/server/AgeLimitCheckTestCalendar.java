/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validators.server;

import de.knightsoftnet.validators.shared.beans.AgeLimitTestBeanCalendar;
import de.knightsoftnet.validators.shared.testcases.AgeLimitTestCasesCalendar;

import org.junit.Test;

/**
 * age limit check test with Calendar.
 *
 * @author Manfred Tremmel
 *
 */
public class AgeLimitCheckTestCalendar extends AbstractValidationTest<AgeLimitTestBeanCalendar> {

  /**
   * empty value is allowed.
   */
  @Test
  public final void testEmptyAgeIsAllowed() {
    super.validationTest(AgeLimitTestCasesCalendar.getEmptyTestBean(), true, null);
  }

  /**
   * correct ages are allowed.
   */
  @Test
  public final void testCorrectAgesAreAllowed() {
    for (final AgeLimitTestBeanCalendar testBean : AgeLimitTestCasesCalendar
        .getCorrectTestBeans()) {
      super.validationTest(testBean, true, null);
    }
  }

  /**
   * wrong ages are not allowed.
   */
  @Test
  public final void testWrongAgesSizeAreWrong() {
    for (final AgeLimitTestBeanCalendar testBean : AgeLimitTestCasesCalendar.getWrongTestBeans()) {
      super.validationTest(testBean, false,
          "de.knightsoftnet.validators.shared.impl.AgeLimitCheckValidatorForCalendar");
    }
  }
}
