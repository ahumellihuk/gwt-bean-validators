package de.knightsoftnet.mtwidgets.client.jswrapper;

import elemental.dom.Element;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true)
public class IntersectionObserverEntry {

  @JsProperty
  public Element target;

  @JsProperty
  public double intersectionRatio;
}
