package de.knightsoftnet.mtwidgets.client.jswrapper;

import elemental.dom.Element;
import jsinterop.annotations.JsType;

@JsType(isNative = true)
public abstract class IntersectionObserver {

  /**
   * Default constructor.
   */
  // @JsConstructor
  // public IntersectionObserver(final EventListenerCallback onIntersection,
  // final JsonObject config) {}

  /**
   * Observes the target.
   *
   * @param target the target
   */
  @SuppressWarnings("unusable-by-js")
  public native void observe(final Element target);

  /**
   * Removes the target from observation.
   *
   * @param target the target
   */
  @SuppressWarnings("unusable-by-js")
  public native void unobserve(final Element target);

  /**
   * Disconnects.
   */
  public native void disconnect();

  /**
   * Takes records.
   *
   * @return the records
   */
  public native Object takeRecords();
}
