/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.mtwidgets.client.ui.widget;

import de.knightsoftnet.mtwidgets.client.jswrapper.IntersectionObserver;
import de.knightsoftnet.mtwidgets.client.jswrapper.IntersectionObserverEntry;
import de.knightsoftnet.mtwidgets.client.jswrapper.ObserverEventListenerCallback;

import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.user.client.ui.Image;

import elemental.html.ImageElement;

/**
 * Image lazy loading is based on the GWT {@link Image} and provides all the functionality inside.
 * The difference is, image urls are put into the image tag when image is in the viewport so no
 * images are loaded by the browser, which are not visible.
 *
 * @author Manfred Tremmel
 *
 */
public class ImageLazyLoading extends Image {

  private static final String DATA_SRC = "data-src";

  private static IntersectionObserver observer =
      createIntersectionObserver(element -> displayImage(element));

  public ImageLazyLoading() {
    super();
  }

  public ImageLazyLoading(final Element pelement) {
    super(pelement);
  }

  public ImageLazyLoading(final ImageResource presource) {
    super(presource);
  }

  public ImageLazyLoading(final SafeUri purl, final int pleft, final int ptop, final int pwidth,
      final int pheight) {
    super(purl, pleft, ptop, pwidth, pheight);
  }

  public ImageLazyLoading(final SafeUri purl) {
    super(purl);
  }

  public ImageLazyLoading(final String purl, final int pleft, final int ptop, final int pwidth,
      final int pheight) {
    super(purl, pleft, ptop, pwidth, pheight);
  }

  public ImageLazyLoading(final String purl) {
    super(purl);
  }

  @Override
  public void setUrl(final SafeUri purl) {
    getElement().setAttribute(DATA_SRC, purl.asString());
    if (observer == null) {
      super.setUrl(purl);
    } else {
      observer.observe((elemental.dom.Element) getElement());
    }
  }

  public static native IntersectionObserver createIntersectionObserver(
      final ObserverEventListenerCallback callback) /*-{
    if (!('IntersectionObserver' in window)) {
      return null;
    }
    var observerConf = {
      rootMargin: '50px 0px',
      threshold: 0.01
    };
    return new IntersectionObserver(callback, observerConf);
  }-*/;

  private static void displayImage(final IntersectionObserverEntry[] observerList) {
    for (final IntersectionObserverEntry observerEntry : observerList) {
      if (observerEntry.intersectionRatio > 0.0 //
          && observerEntry.target instanceof ImageElement) {
        final ImageElement imageElement = (ImageElement) observerEntry.target;
        imageElement.setSrc(imageElement.getAttribute(DATA_SRC));
        observer.unobserve(imageElement);
      }
    }
  }
}
