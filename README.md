gwt-bean-validators-parent
==========================

parent project of the modules

* [mt-bean-validators](./mt-bean-validators)
* [gwt-bean-validators](./gwt-bean-validators)
* [gwtp-spring-integration-shared](./gwtp-spring-integration-shared)
* [gwtp-spring-integration-server](./gwtp-spring-integration-server)
* [gwtp-spring-integration-client](./gwtp-spring-integration-client)
* [gwtp-dynamic-navigation](./gwtp-dynamic-navigation)
* [gwt-bean-validators-restygwt-jaxrs](./gwt-bean-validators-restygwt-jaxrs)
* [gwt-bean-validators-spring-gwtp](./gwt-bean-validators-spring-gwtp)
* [gwt-mt-widgets](./gwt-mt-widgets)
* [gwt-mt-widgets-restygwt-jaxrs](./gwt-mt-widgets-restygwt-jaxrs)
* [gwt-mt-widgets-spring-gwtp](./gwt-mt-widgets-spring-gwtp)

for more informations look at the modules